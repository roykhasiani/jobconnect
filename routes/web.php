<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@index');
Route::get('/index', 'PagesController@index');
Route::get('/candidate_reg', 'PagesController@candidate_reg');
Route::get('/company_reg', 'PagesController@company_reg');
Route::get('/login', 'PagesController@login');

//handles login
Route::get('/home', 'CompaniesController@index');

//handles dashboard
Route::get('/dashboard/my', 'CompaniesController@dashboard');

Route::resource('companies', 'CompaniesController');
Route::resource('candidates', 'CandidatesController');

//user authentication
Auth::routes();
