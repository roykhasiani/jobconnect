@extends('layouts.app')

{{-- SHOW THE BASIC HOME WELCOME JUMBOTRON WITH LOGIN AND REGISTER BUTTONS --}}
@section('content')

    <div class="w3-container">
       <br>
        <h1 class="w3-center w3-padding"><br><b>Potential Employers' List</b></h1>
        <h4 class="w3-center w3-padding w3-text-blue">Sorted by Name From A-Z</h4> 
        <hr>
         <input oninput="w3.filterHTML('#id01', 'li', this.value)" class="w3-input w3-card-4 w3-light-blue" style="width:50%; margin: auto;
            width: 70%; border: 3px solid-blue; padding: 10px;" placeholder="Click here to Search by Company or by Location of Company..">
    @if(!Auth::guest())
        @if(Auth::user()->id == $dashboard->user_id)
         @if(count($dashboard)> 0)
            <ul id="id01" class="w3-ul w3-card-4" style="width:50%; margin: auto;
            width: 70%; border: 3px solid-blue; padding: 10px;">
                @foreach($dashboard as $company)
                    <li class="w3-bar">
                    <a href="/dashboard/{{$company->id}}" style="text-decoration:none;">
                    <img src="{{URL::asset('images/company.png')}}" class="w3-bar-item w3-circle w3-hide-small" style="width:85px">
                    <div class="w3-bar-item">
                    <span class="w3-large w3-text-blue">{{$company->name}}</span><br>
                    <span>{{$company->location}}</span>
                    </div>
                    </a>
                    </li>
                @endforeach
                
            </ul>
            <div class="w3-center" style="width:50%; margin: auto;
            width: 12%; border: 3px solid-blue; padding: 10px;">
                <div class="pagination"> {!!$dashboard->links()!!}</div>
             {{-- <div class="w3-bar"> {!!$dashboard->links()!!}</div> --}}
            </div>
    </div>
        @else
            <ul class="w3-ul w3-card-4 w3-center" style="width:50%; margin: auto;
            width: 70%; border: 3px solid-blue; padding: 10px;">
                
            <h4 class="w3-text-blue">Sorry, No dashboard to display</h4>
            </ul>
        @endif
        @endif
        @endif
</div>
@endsection
<script type="text/javascript">
$('#pagination-demo').twbsPagination({
  
  visiblePages: 2,
  onPageClick: function (event, page) {
    $('#page-content').text('Page ' + page);
  }
});




</script>