@extends('layouts.app')

{{-- SHOW THE BASIC HOME WELCOME JUMBOTRON WITH LOGIN AND REGISTER BUTTONS --}}
@section('content')
    <div class="w3-container">
       <br>
       <h1 class="w3-center w3-padding"><br><b>Potential Employees' List</b></h1>
        <h4 class="w3-center w3-padding w3-text-blue">Sorted by Name From A-Z</h4> 
        <hr>
        
        <input oninput="w3.filterHTML('#id01', 'li', this.value)" class="w3-input w3-card-4 w3-light-blue" style="width:50%; margin: auto;
            width: 70%; border: 3px solid-blue; padding: 10px;" placeholder="Click here to Search by University or Name..">
        @if(count($candidates)> 0)
            <ul id="id01" class="w3-ul w3-card-4" style="width:50%; margin: auto;
            width: 70%; border: 3px solid-blue; padding: 10px;">
                @foreach($candidates as $candidate)
                    <li class="w3-bar">
                    <a href="/candidates/{{$candidate->id}}" style="text-decoration:none;">
                    <img src="{{URL::asset('images/reg.png')}}" class="w3-bar-item w3-circle w3-hide-small" style="width:85px">
                    <div class="w3-bar-item">
                    <span class="w3-large w3-text-blue">{{$candidate->username}}</span><br>
                    <span>{{$candidate->university}} :-</span>
                    <span>{{$candidate->course}}</span>
                    </div>
                </a>
                    </li>
                @endforeach
                
            </ul>
            <div class="w3-center" style="width:50%; margin: auto;
            width: 12%; border: 3px solid-blue; padding: 10px;">
                <div class="pagination"> {!!$candidates->links()!!}</div>
             {{-- <div class="w3-bar"> {!!$companies->links()!!}</div> --}}
            </div>
    </div>
        @else
            <ul class="w3-ul w3-card-4 w3-center" style="width:50%; margin: auto;
            width: 70%; border: 3px solid-blue; padding: 10px;">
                
            <h4 class="w3-text-blue">Sorry, No companies to display</h4>
            </ul>
        @endif
</div>
@endsection