

@extends('layouts.app')

{{-- SHOW THE BASIC HOME WELCOME JUMBOTRON WITH LOGIN AND REGISTER BUTTONS --}}
@section('content')
    <div class="w3-card-4 w3-animate-zoom w3-round-large w3-border-black" style="max-width:800px; margin-left:auto; margin-right:auto; margin-top:50px;">
            <div class="w3-center"><br>
                <img src="{{URL::asset('images/reg.png')}}" alt="User Registration" style="width:20%" class="w3-circle w3-margin-top">
            </div>


                <h2 style="text-align: center" class=" w3-text-blue w3-border-bottom w3-border-blue"><b>SUMMARY OF YOUR C.V.</b></h2> <br> <h4 style="text-align: center" class=" w3-text-blue w3-border-bottom w3-border-blue"><b>Note: This will attract Employers to look at your attached Resume</b><h4><br>
                {!! Form::open(['action'=> ['CandidatesController@update', $candidates->id ], 'method'=>'POST', 'enctype'=>'multipart/form-data']) !!}
                <div class="w3-row-padding">
                    <div class="w3-half">
                        {{Form::label('username', 'Official Name:', ['class'=>'w3-text-blue'])}}
                        {{Form::text('username', $candidates->username, ['class'=>'w3-input w3-border w3-round-large w3-border-blue', 'placeholder'=>'First Name Surname Last Name'])}}
                        
                    </div>
                    

                    <div class="w3-half">
                        {{Form::label('email', 'Official Email:', ['class'=>'w3-text-blue'])}}
                        {{Form::email('email', $candidates->email, ['class'=>'w3-input w3-border w3-round-large w3-border-blue', 'placeholder'=>''])}}
                        
                    </div>

                    <div class="w3-half">
                        {{Form::label('location', 'Home County:', ['class'=>'w3-text-blue'])}}
                        {{Form::text('location', $candidates->location, ['class'=>'w3-input w3-border w3-round-large w3-border-blue', 'placeholder'=>''])}}
                        
                    </div>

                    
                    <div class="w3-half">
                        {{Form::label('phone', 'Official Phone Number:', ['class'=>'w3-text-blue'])}}
                        {{Form::number('phone', $candidates->phone, ['class'=>'w3-input w3-border w3-round-large w3-border-blue', 'placeholder'=>'e.g. 0711223344'])}}
                    </div>

                    <div class="w3-half">
                        {{Form::label('age', 'Age:', ['class'=>'w3-text-blue'])}}
                        {{Form::text('age', $candidates->age, ['class'=>'w3-input w3-border w3-round-large w3-border-blue', 'placeholder'=>''])}}
                    </div>

                    
                    <div class="w3-half">
                        {{Form::label('gender', 'Gender:', ['class'=>'w3-text-blue'])}}
                        {{Form::radio('gender', 'Male')}}
                        {{Form::label('gender', ': Male')}}
                        {{Form::radio('gender', 'Female')}}
                        {{Form::label('gender', ': Female')}}
                    </div>

                    
                    <div class="w3-half">
                        {{Form::label('university', 'University Attended:', ['class'=>'w3-text-blue'])}}
                        {{Form::text('university', $candidates->university, ['class'=>'w3-input w3-border w3-round-large w3-border-blue', 'placeholder'=>''])}} 
                    </div>

                    
                    <div class="w3-half">
                        {{Form::label('course', 'Course Taken:', ['class'=>'w3-text-blue'])}}
                        {{Form::text('course', $candidates->course, ['class'=>'w3-input w3-border w3-round-large w3-border-blue', 'placeholder'=>'At Highest Level Of Study'])}}                        
                    </div>

                    
                    <div class="w3-half">
                        {{Form::label('gradyear', 'Graduation Year:', ['class'=>'w3-text-blue'])}}
                        {{Form::number('gradyear', $candidates->gradyear, ['class'=>'w3-input w3-border w3-round-large w3-border-blue', 'placeholder'=>''])}}
                         <br>
                    </div>

                   
                    <div class="w3-center">
                        {{Form::label('previous_company', 'Previous Company (N/A if none):', ['class'=>'w3-text-blue'])}}
                        {{Form::text('previous_company', $candidates->previous_company, ['class'=>'w3-input w3-border w3-round-large w3-border-blue', 'placeholder'=>''])}}                        
                    </div><br><br>

                
                    <div class="">
                        {{Form::label('interests', 'Fill in Your ACADEMIC INTERESTS in the Editor Below Remember to be as CONSCISE AND PRECISE AS POSSIBLE to MAXIMIZE YOUR CHANCES OF EMPLOYMENT (Preferred maximum number of words is 150):', ['class'=>'w3-text-blue'])}}
                        {{Form::textarea('interests', $candidates->interests, ['id'=>'article-ckeditor', 'class'=>'w3-input w3-border w3-round-large w3-border-blue', 'placeholder'=>''])}}
                    </div>

                    
                    <div class="w3-half">
                        {{Form::label('availability', 'Your Availability:', ['class'=>'w3-text-blue'])}}
                        {{Form::radio('availability', 'Full-Time')}}
                        {{Form::label('availability', ': Full-Time')}}
                        {{Form::radio('availability', 'Part-Time')}}
                        {{Form::label('availability', ': Part-Time')}}
                    </div>

                    <div class="w3-half">
                        {{Form::label('resume', 'Upload Your Resume (ONLY .PDF FILE ACCEPTED):', ['class'=>'w3-text-blue'])}}
                        {{Form::file('resume', ['class'=>'w3-input w3-border w3-round-large w3-border-blue', 'placeholder'=>'e.g. 0711223344', 'accept'=>'.pdf'])}}                       
                    </div> 
                    {{Form::hidden('_method', 'PUT')}}
                    {{Form::submit('Submit', ['class'=>'w3-button w3-block w3-round-large w3-blue w3-section w3-padding'])}}
            </div>
        {!! Form::close() !!}
        
        </div>

@endsection