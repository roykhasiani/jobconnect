@extends('layouts.app')


@section('content')
    <div class="w3-container">

            <div class="w3-card-4 w3-round-large w3-border-black w3-center" style="max-width:600px; margin-left:auto; margin-right:auto; margin-top:50px;">
            <div class="w3-center"><br>
                <img src="{{URL::asset('images/company.png')}}" alt="" style="width:30%" class="w3-circle w3-margin-top">
            </div>
                <div class="w3-section">
                    <h3  class="w3-text-blue w3-border-bottom w3-border-blue"><b>{{$candidates->username}}'s Profile</b></h3><br>
                    <label class="w3-text-blue"><b>Name : {{$candidates->username}}</b></label><br>                    
                    <label class="w3-text-blue"><b>Location : {{$candidates->location}}</b></label><br>                    
                    <label class="w3-text-blue"><b>Contact Email : {{$candidates->email}}</b></label><br>
                    <label class="w3-text-blue"><b>Age : {{$candidates->age}} Years Old</b></label><br>
                    <label class="w3-text-blue"><b>Mobile Number : 0{{$candidates->phone}}</b></label><br>
                    <label class="w3-text-blue"><b>University Attended : {{$candidates->university}}</b></label><br>
                    <label class="w3-text-blue"><b>Course Taken : {{$candidates->course}}</b></label><br>
                    <label class="w3-text-blue"><b>Graduated in : {{$candidates->gradyear}}</b></label><br>
                    <label class="w3-text-blue"><b>Previously Worked at : {{$candidates->previous_company}}</b></label><br>
                    <label class="w3-text-blue"><b>Willing to work : {{$candidates->availability}}</b></label><br>
                    <label class="w3-text-blue"><b>Fields of Interest : {!!$candidates->interests!!}</b></label><br>
                    <a href="/candidates/{{$candidates->id}}" download="{{$candidates->resume}}" class="w3-bar-item w3-button w3-blue w3-center" style="text-decoration:none;"><b>Go To C.V.       <i class="fa fa-download"></i></b></a><br>
                    <small class="w3-text-blue">Updated at: {{$candidates->updated_at}}</small><br><br>
                @if(!Auth::guest())
                    @if(Auth::user()->id == $candidates->user_id)
                    <div class="w3-center">
                        <a href="/candidates/{{$candidates->id}}/edit" class="w3-bar-item w3-button w3-blue w3-center" style="max-width:600px; margin-left:auto; margin-right:auto;text-decoration:none;margin-bottom:5px"><b>Edit       <i class="fa fa-edit"></i></b></a><br>
                        {!! Form::open(['action'=>['CandidatesController@destroy', $candidates->id], 'method'=>'POST', 'class'=>'w3-center']) !!}
                            {!! Form::hidden('_method', 'DELETE') !!}
                            {!! Form::submit('Delete', ['class'=>'btn btn-danger'])!!}
                        {!! Form::close() !!}
                     </div>
                     @endif
                @endif
                <a href="/candidates" class="w3-bar-item w3-button w3-blue w3-center" style="text-decoration:none;"><b>Back       <i class="fa fa-backward"></i></b></a>
                    <br><br><br><br>
                 </div>
            </div>
    </div>

@endsection


