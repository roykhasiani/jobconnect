-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 07, 2019 at 08:24 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jobconnect`
--

-- --------------------------------------------------------

--
-- Table structure for table `candidates`
--

CREATE TABLE `candidates` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` int(11) NOT NULL,
  `age` int(11) NOT NULL,
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `university` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `course` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gradyear` int(11) NOT NULL,
  `previous_company` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `interests` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `availability` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `resume` blob NOT NULL,
  `user_id` int(10) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `candidates`
--

INSERT INTO `candidates` (`id`, `username`, `email`, `location`, `phone`, `age`, `gender`, `university`, `course`, `gradyear`, `previous_company`, `interests`, `availability`, `resume`, `user_id`, `created_at`, `updated_at`) VALUES
(3, 'Humphery Lipesa', 'hlipesa@gmail.com', 'Mombasa', 718518933, 24, 'Male', 'Moi University Mombasa Campus', 'Computer Science', 2018, 'Moi University - Associate Developer', '<ol>\r\n	<li><em><strong>Computer Architecture</strong></em></li>\r\n	<li><em><strong>REST Technologies</strong></em></li>\r\n	<li><em><strong>Java, PhP, Python</strong></em></li>\r\n</ol>', 'Part-Time', 0x50726f67726573735265706f72742d3039343338325f325f313533313133333634322e706466, 7, '2018-07-09 07:54:02', '2018-07-09 07:54:02'),
(4, 'Cynthia Walumbe', 'cwalumbe@gmail.com', 'Eldoret', 743899043, 20, 'Female', 'Kenyatta University Main Campus', 'Fashion and Design', 2024, 'N/A', '<ol>\r\n	<li><em><strong>Interior Design</strong></em></li>\r\n	<li><em><strong>Fashion</strong></em></li>\r\n	<li><em><strong>African Wear</strong></em></li>\r\n</ol>', 'Part-Time', 0x50726f67726573735265706f72742d3039343338325f313533313133333834332e706466, 7, '2018-07-09 07:57:23', '2018-07-09 07:57:23'),
(7, 'Cornelius Mambili', 'cmambili@gmail.com', 'Bungoma', 717486841, 29, 'Male', 'Strathmore University', 'Bachelors Of Commerce', 2017, 'N/A', '<ol>\r\n	<li><em><strong>Accounting</strong></em></li>\r\n	<li><em><strong>Finance</strong></em></li>\r\n	<li><em><strong>Mathematical Modelling</strong></em></li>\r\n</ol>', 'Full-Time', 0x50726f67726573735265706f72742d3039343338325f335f313533313230333432392e706466, 7, '2018-07-10 03:12:06', '2018-07-10 03:17:09'),
(8, 'Okumu Peter Braxton Omondi', 'peterbraxton@hotmail.com', 'Kisumu', 718872551, 21, 'Male', 'Strathmore University', 'BBIT', 2020, 'N/A', '<p>Sports</p>', 'Full-Time', 0x50726f67726573735265706f72742d3039343338325f335f313533313232363531312e706466, 1, '2018-07-10 09:41:52', '2018-07-10 09:41:52');

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE `companies` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `interests` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`id`, `name`, `location`, `email`, `interests`, `created_at`, `updated_at`, `user_id`) VALUES
(1, 'Safaricom Ltd', 'Westlands, Nairobi', 'info@safaricomllc.com', '<p>Telecommunications, Mobile money, IT, Programming</p>', '2018-06-09 04:33:30', '2018-07-09 05:39:50', 1),
(2, 'KPMG Inc.', 'Mombasa Rd. Nairobi', 'info@kpmgafrica.com', 'Energy, IT, Business Intelligence, Disruptive Tech', '2018-06-09 04:38:07', '2018-06-09 04:38:07', 2),
(3, 'Deloitte Ltd.', 'Nairobi School Tana House, Nairobi', 'info@deloittekenya.org', '<p>Providing audit, Tax, Legal, Financial advisory, Risk advisory, and Consulting services</p>', '2018-06-10 04:03:25', '2018-06-21 21:49:49', 3),
(4, 'Unilever Africa', 'Factory St, Nairobi', 'info@unileverafrica.com', '<p>Sustainable products, Health, Environmental Well-being, Business Branding</p>', '2018-06-10 04:38:17', '2018-06-11 09:45:22', 4),
(5, 'Strathmore University', 'Madaraka Ole Sangale', 'info@strathmore.edu', '<p>Education</p>\r\n\r\n<p>Mentoring</p>\r\n\r\n<p>Sports</p>\r\n\r\n<p>Wholistic growth</p>', '2018-06-25 15:50:34', '2018-06-25 15:50:34', 6);

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `faculty` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_06_09_070935_create_candidates_table', 1),
(4, '2018_06_09_072541_create_companies_table', 2),
(5, '2018_06_10_184739_add_foreign_key_to_candidates_table', 3),
(6, '2018_06_11_122822_create_courses_table', 4);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('rmurwa@gmail.com', '$2y$10$dReIqDQIFdhVLZdBXpwZJOEwX/GiEjwsC9dSmAi/GvnJ6ChSRTEmi', '2018-06-25 14:30:45');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Roy Murwa', 'rmurwa@gmail.com', '$2y$10$FsrjrRw0A.YasyFRArzbeu4.FRqi1jMm9RHwrQhUPAvnlADoQCe4C', 'lnRXE5RomLx8Qfm2mAXW7CfQt81CrpWPdgjmPGNvcnoBTZHeKHYuEciTrVH3', '2018-06-10 08:23:27', '2018-06-10 08:23:27'),
(2, 'Hum Murwa', 'hum@gmail.com', '$2y$10$S3NHxKB3hatrPs6/BX68Ce08lBf0AkiOgIW8H3O7htN6Egy6x1RUm', '1cLARcLKPgxNLyWpAfZFPRbY0QmXkdgcYsvnSFrfSy1RWKqguxrwmxFaYoXV', '2018-06-10 09:00:20', '2018-06-10 09:00:20'),
(3, 'Admin', 'admin@gmail.com', '$2y$10$ZenA00bnd9/ypn1K7DYSTuEMyA41qq248YgJdoJJDOFCfbRAYwAHi', 'sGF2LNMJfAI5jOeWj66mJwxvAnzLzfVLowscmQih1Xc8pmvVzAhGKUwtQXcI', '2018-06-10 15:38:41', '2018-06-10 15:38:41'),
(4, 'Unilever Kenya Ltd.', 'info@unileverafrica.com', '$2y$10$Tj1KOGUa0nW.WE2lTRjxkOGWVP/v5GuiZN9OtsxgN57RmVId.D2kW', NULL, '2018-06-10 16:03:32', '2018-06-10 16:03:32'),
(5, 'Theo Philus', 'theo@theo.com', '$2y$10$kcZ4nXe1F1yKdrK9jpQAnua8OgI5Kal4Rix3ACDe8lkx5xYzdDNoS', 'R9i8odHgUmx7EOiXQ4BX5ooKxLtq1Z8KDAhQ8OxdBsfLrNZhYMapvX4aen7T', '2018-06-11 09:17:37', '2018-06-11 09:17:37'),
(6, 'Strathmore University', 'strathmore@gmail.com', '$2y$10$BT7KJh4dLnK4UJ5lnQq9cu7AgGi.h6ZqrFUS6u/WIqJctdUnWa5vS', 'sSTuSVSbSDE06UMrYuTc6popNQWuiIQLQaNXlFlXY4HNcMuY3AQstbPFMzwi', '2018-06-25 15:26:03', '2018-06-25 15:26:03'),
(7, 'matwice', 'cmambili@gmail.com', '$2y$10$LXp9ldlf6ngnp0IKICfRKeQ9Q.Fn6Jb4.Qeh/A6A8rMeuY6NgDaPe', 'OhKjrkCPLVv8lcPjXBREuAbZqBW1UFM78CVpK24ndkBAB8GmdTH1kKj5S49S', '2018-07-09 06:12:38', '2018-07-09 06:12:38'),
(8, 'Flora Olendo', 'folendo@gmail.com', '$2y$10$/7fDzmMTfeizdKDvccweaeX.2YcvL0b.F1efP5XEUKkfBezs5QbHS', NULL, '2018-07-11 01:31:09', '2018-07-11 01:31:09');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `candidates`
--
ALTER TABLE `candidates`
  ADD PRIMARY KEY (`id`),
  ADD KEY `candidates_user_id_index` (`user_id`),
  ADD KEY `user_id_2` (`user_id`),
  ADD KEY `user_id` (`user_id`) USING BTREE;

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `candidates`
--
ALTER TABLE `candidates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `companies`
--
ALTER TABLE `companies`
  ADD CONSTRAINT `user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
