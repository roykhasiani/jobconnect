<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Candidate;
class DownloadsController extends Controller
{
    public function downfunc($id){
        $candidate_id = Candidate::find($id);
        $candidate_id = $id;
        $downloads = DB::table('candidates')->get('resume');
        return view('download.viewfile', compact('downloads'));
    }
}
