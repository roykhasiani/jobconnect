<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use App\Candidate;

class CandidatesController extends Controller
{

        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        /**require authentication on all company pages except the index page
        which simply displays the listed companies
        */
        $this->middleware('auth', ['except'=>['index']]);
    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $candidate = Candidate::orderBy('username', 'asc')->paginate(2);
        return view('candidates.index')->with('candidates', $candidate);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('candidates.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['username'=>'required', 'email'=>'required', 'location'=>'required', 'phone'=>'required', 'age'=>'required', 'gender'=>'required', 'university'=>'required', 'course'=>'required', 'gradyear'=>'required', 'previous_company'=>'required', 'interests'=>'required', 'availability'=>'required', 'resume'=>'required']);
        
        //handle file upload
        if($request->hasFile('resume')){
            //get file name with extension
            $filenameWithExt = $request->file('resume')->getClientOriginalName();

            //get file name only
            $filename = pathInfo($filenameWithExt, PATHINFO_FILENAME);
            //get file extension only
            $extension = $request->file('resume')->getClientOriginalExtension();
            //file name to store
            $fileNameToStore = $filename.'_'.time().'.'.$extension;
            //upload image
            $path = $request->file('resume')->storeAs('public/resumes', $fileNameToStore);
             }else{
                    $fileNameToStore = 'NORESUME.PDF';
                }
                
                $candidate = new Candidate;
                $candidate->username = $request->input('username');
                $candidate->email
                = $request->input('email'
                );
                $candidate->location
                = $request->input('location'
                );
                $candidate->phone
                = $request->input('phone'
                );
                $candidate->age
                = $request->input('age'
                );
                $candidate->gender
                = $request->input('gender'
                );
                $candidate->university
                = $request->input('university'
                );
                $candidate->course
                = $request->input('course'
                );
                $candidate->gradyear
                = $request->input('gradyear'
                );
                $candidate->previous_company
                = $request->input('previous_company'
                );
                $candidate->interests
                = $request->input('interests'
                );
                $candidate->resume
                = $fileNameToStore;

                $candidate->availability
                = $request->input('availability'
                );

                $candidate->user_id = auth()->user()->id;
                $candidate->save();
                return redirect('/candidates')->with('success', 'Candidate Added Successfully');
            }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $candidates = Candidate::find($id);
        return view('candidates.show')->with('candidates', $candidates);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $candidate = Candidate::find($id);

        //check for correct user
        if(auth()->user()->id !== $candidate->user_id){
            return redirect('/candidates')->with('error', 'Unauthorized page');
        }else{
            return view('candidates.edit')->with('candidates', $candidate);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $this->validate($request, ['username'=>'required', 'email'=>'required', 'location'=>'required', 'phone'=>'required', 'age'=>'required', 'gender'=>'required', 'university'=>'required', 'course'=>'required', 'gradyear'=>'required', 'previous_company'=>'required', 'interests'=>'required', 'availability'=>'required', 'resume'=>'required']);
        
        //handle file upload
        if($request->hasFile('resume')){
            //get file name with extension
            $filenameWithExt = $request->file('resume')->getClientOriginalName();

            //get file name only
            $filename = pathInfo($filenameWithExt, PATHINFO_FILENAME);
            //get file extension only
            $extension = $request->file('resume')->getClientOriginalExtension();
            //file name to store
            $fileNameToStore = $filename.'_'.time().'.'.$extension;
            //upload image
            $path = $request->file('resume')->storeAs('public/resumes', $fileNameToStore);
        }
        
            $candidate = Candidate::find($id);
             $candidate->username = $request->input('username');
             $candidate->email
            = $request->input('email'
            );
            $candidate->location
            = $request->input('location'
            );
            $candidate->phone
            = $request->input('phone'
            );
            $candidate->age
            = $request->input('age'
            );
            $candidate->gender
            = $request->input('gender'
            );
            $candidate->university
            = $request->input('university'
            );
            $candidate->course
            = $request->input('course'
            );
            $candidate->gradyear
            = $request->input('gradyear'
            );
            $candidate->previous_company
            = $request->input('previous_company'
            );
            $candidate->interests
            = $request->input('interests'
            );

            if($request->hasFile('resume')){
                $candidate->resume = $fileNameToStore;
            }

            $candidate->availability
            = $request->input('availability'
            );

            $candidate->user_id = auth()->user()->id;
            $candidate->save();
            return redirect('/candidates')->with('success', 'Candidate Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $candidates = Candidate::find($id);

        if($candidates->resume != 'NORESUME.PDF'){
            //delete image
            Storage::delete('public/resumes'.$candidates->resume);
        }

        $candidates->delete();
        return redirect('/candidates')->with('success', 'Candidate Deleted Successfully');
    }


    
}
